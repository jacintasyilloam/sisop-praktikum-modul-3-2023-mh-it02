# Soal Shift Modul 3
## Sistem Operasi 2023

**Sisop girlbosses of IT02:**
| Nama | NRP |
| ---------------------- | ---------- |
| Azzahra Sekar Rahmadina | 5027221035 |
| Jacinta Syilloam | 5027221036 |
| Stephanie Hebrina Mabunbun Simatupang | 5027221069 |

## List of Contents
- [Soal 1](#soal-1) 
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)
- [Soal 4](#soal-4)


## Soal 1
###
**Program named "belajar.c" that performs matrix multiplication. To allow for varying matrix sizes, the first matrix's size should be [2]×2, and the second matrix should be 2×[2]. The matrix contents will be defined within the code and will consist of random numbers. The range for values in the first matrix is 1-4 (inclusive), and for the second matrix, it's 1-5 (inclusive). Display the result of the matrix multiplication on the screen.**


```
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

//matriks 2x2 karena it02

int main() {
    srand(time(NULL));

    int matrix1[2][2]; //masukin nilai baris kolom matriks1
    int matrix2[2][2]; //masukin nilai baris kolom matriks2
    int result[2][2]; //hasilnya ordo 2x2

    // matriks 1 dimana angkanya 1-4
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            matrix1[i][j] = rand() % 4 + 1;
        }
    }
    
    printf("Matrix 1:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d ", matrix1[i][j]);
        }
        printf("\n");
    }

    // matriks 1 dimana angkanya 1-5
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            matrix2[i][j] = rand() % 5 + 1;
        }
    }
    
    printf("\nMatrix 2:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    }

    // perkalian matriks ordo 2x2
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            result[i][j] = 0;
            for (int k = 0; k < 2; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }
    
    //hasil A
    printf("\nResult matriks 2 x 2:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

```

- It includes necessary header files for standard input/output, random number generation, and time functions, as well as headers for inter-process communication using shared memory (although the shared memory functionality is not used in this code).

- The code initializes a random number generator with the current time to ensure different random numbers are generated each time the program is run.

- Three 2x2 integer arrays, matrix1, matrix2, and result, are declared to store the matrices and their multiplication result.

- The code fills matrix1 and matrix2 with random integers within specific ranges:


1. matrix1 contains integers between 1 and 4.

1. matrix2 contains integers between 1 and 5.
- It then prints the contents of matrix1 and matrix2 to the console.

- The code performs matrix multiplication for matrix1 and matrix2 and stores the result in the result matrix using nested loops. This is a standard matrix multiplication algorithm that multiplies the elements of the matrices and sums the products to fill the resulting matrix.

- Finally, it prints the result matrix, which is the product of matrix1 and matrix2.

**Additionally, at Epul's request, subtract 1 from each element in the resulting matrices.**


```
    printf("\nMatriks setelah dikurang 1:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d ", result[i][j] - 1); 
        }
        printf("\n");
    }
```
- Within the nested loops, printf("%d ", result[i][j] - 1); is used to print each element of the result matrix after subtracting 1. Here's what happens in this line:

1. result[i][j] refers to an element of the result matrix at row i and column j.
1. result[i][j] - 1 subtracts 1 from that element.

```
    key_t key = 1234;
    int shmid = shmget(key, sizeof(result), 0666 | IPC_CREAT);
    int (*shared_result)[2] = shmat(shmid, NULL, 0);

    // Copy the result to shared memory after -1
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            shared_result[i][j] = result[i][j] - 1;
        }
    }

    // ngelepasin ke share memory
    shmdt(shared_result);

    return 0;
}
```
```key_t key = 1234```: This line initializes a key, which is used to identify the shared memory segment. In this case, the key is set to 1234.

```int shmid = shmget(key, sizeof(result), 0666 | IPC_CREAT)```: This line creates or accesses a shared memory segment using the shmget function. It takes three arguments:

`key` is the identifier for the shared memory segment.

`sizeof(result)` specifies the size of the shared memory segment, which is the size of the result matrix.

`0666 | IPC_CREAT` sets the permissions for the shared memory segment, allowing read and write access to everyone (0666) and creating the segment if it doesn't exist (IPC_CREAT).

`int (*shared_result)[2] = shmat(shmid, NULL, 0)`: This line attaches the shared memory segment to the current process using the shmat function. It creates a pointer shared_result that points to the shared memory segment. The shmid is the identifier of the shared memory segment. The NULL argument specifies that the operating system should choose a suitable address for the shared memory, and 0 indicates no special flags.

The code then enters a nested loop with two `for` loops, similar to the previous code, to copy the modified `result` matrix to the shared memory. It subtracts 1 from each element of result and stores the modified values in shared_result.

After copying the data to shared memory, `shmdt(shared_result)` is used to detach the shared memory segment from the current process when it's no longer needed.

Finally, the `return 0` statement is included to terminate the program.

**Here's the output for belajar.c**
![Output 1](OutputPics/belajar.png)

<br>


**Program named "yang.c" will retrieve the result variable from the matrix multiplication performed in the previous program, belajar.c. The result of subtracting the matrix multiplication will be transposed, and the result will be displayed.**

```
int main() {
    int shmid;
    int size = sizeof(int[2][2]);
    key_t key = 1234;

    shmid = shmget(key, size, 0666);
    int (*shared_matrix)[2] = shmat(shmid, NULL, 0);

    int result[2][2];
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            result[i][j] = shared_matrix[j][i]; //dibalik soalnya untuk transpose
        }
    }
    shmdt(shared_matrix);

    // Print hasil matrix
    printf("Matriks:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

```
This C program utilizes shared memory to transpose a 2x2 integer matrix stored in the shared memory segment. It first establishes a shared memory segment with a given key and size, attaches to it, and then reverses the elements to achieve the transpose operation, storing the result in a local matrix. Afterward, it detaches from the shared memory and prints the transposed matrix. This code exemplifies interprocess communication through shared memory for matrix manipulation.

1. `int shmid;`: Declares an integer variable to store the shared memory segment ID.

2. `int size = sizeof(int[2][2]);`: Determines the size of the shared memory segment required, which is based on a 2x2 integer matrix.

3. `key_t key = 1234;`: Defines a key to identify the shared memory segment. This key is used to access the same shared memory segment from other programs.

4. `shmid = shmget(key, size, 0666);`: Attempts to create or access a shared memory segment using the specified key and size. The `0666` permission allows read and write access to the shared memory.

5. `int (*shared_matrix)[2] = shmat(shmid, NULL, 0);`: Attaches the program to the shared memory segment identified by `shmid`. It casts the shared memory segment as a 2x2 integer matrix, making it accessible for read and write operations.

6. `int result[2][2];`: Declares a 2x2 integer matrix to store the transposed matrix.

7. The nested `for` loops iterate through the elements of the `shared_matrix`, transposing the elements and storing them in the `result` matrix.

8. `shmdt(shared_matrix);`: Detaches the program from the shared memory segment once the operation is completed.

9. The program then proceeds to print the transposed matrix in a readable format using nested `for` loops.



**After being displayed, Epul wants his sibling to learn more, so for each number in the transpose of that matrix, find its factorial value. Display the results on the screen in a matrix-like format. (Note: You must implement threads and multithreading in the factorial calculations.)**

```
   //menghitung faktorial 
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            thread_args[i][j].row = i;
            thread_args[i][j].col = j;
            thread_args[i][j].value = result[i][j];
        }
    }

    // untuk hitung waktunya
    clock_t start, end;
    double cpu_time_used;
    start = clock();
    calculate_factorial(tid, thread_args);
    end = clock();

    // untuk hitung waktunya
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("\nMaka:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%llu ", factorial_matrix[i][j]);
        }
        printf("\n");
    }

    printf("\nWaktu yang dibutuhkan untuk menghitung faktorial setiap elemen matriks: %f detik\n", cpu_time_used);

    return 0;
}
```

This code calculates the factorials of elements in a 2x2 matrix using multithreading. Here's a step-by-step explanation:

1. A struct named "thread" is defined, which holds information about each matrix element, including its row, column, and value.

2. An unsigned long long array named "factorial_matrix" with a size of 2x2 is declared to store the factorial values.

3. The "calculateFactorial" function is defined, which calculates the factorial of an element based on the provided thread argument. It uses a loop to compute the factorial and stores the result in the "factorial_matrix."

4. The "calculate_factorial" function is defined, which uses multithreading to calculate the factorials of matrix elements. It takes an array of pthread_t objects (tid) and an array of thread structs (thread_args) as arguments. It creates threads for each matrix element using pthread_create and passes the relevant thread argument.

5. After creating all the threads, it waits for them to finish using pthread_join to ensure that all factorials are calculated.

6. A loop populates the "thread_args" array with information about each matrix element (row, column, and value).

7. The code measures the time it takes to calculate the factorials using the clock function. It records the start time, calls the "calculate_factorial" function, and then records the end time.

8. It calculates the CPU time used for the calculation.

9. Finally, it prints the factorial matrix and the time taken to calculate the factorials.

**Here's the output for yang.c**

![Output 1](OutputPics/yang.png)

**In order to satisfy curiosity about the use of threads and multithreading in the previous program, a third C program called "rajin.c" was created. In this program, replicate the actions you performed in "yang.c" but without using threads or multithreading. Make necessary adjustments to demonstrate the differences and comparisons in terms of results and performance between the multithreaded program and the one without it.**
```
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

// Fungsi untuk menghitung faktorial
unsigned long long hitungFaktorial(int num) {
    unsigned long long result = 1;
    for (int i = 1; i <= num; i++) {
        result *= i;
    }
    return result;
}

int main() {
    int shmid;
    int size = sizeof(int[2][2]);
    key_t key = 1234;

    shmid = shmget(key, size, 0666);
    int (*shared_matrix)[2] = (int (*)[2]) shmat(shmid, NULL, 0);

    int result[2][2];
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            result[i][j] = shared_matrix[j][i]; //ditukar biar ke transpose
        }
    }
    shmdt(shared_matrix);

    // Tampilkan matriks hasil perkalian
    printf("Hasil perkalian matriks:\n");
    for(int i=0; i<2; i++) {
        for(int j=0; j<2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    unsigned long long fact_matrix[2][2];
    clock_t start_time, end_time;
    start_time = clock();

    for(int i=0; i<2; i++) {
        for(int j=0; j<2; j++) {
            fact_matrix[i][j] = hitungFaktorial(result[i][j]);
        }
    }
    end_time = clock();

    // hasil faktorial dalam matriks
    printf("\nHasil faktorial matriks:\n");
    for(int i=0; i<2; i++) {
        for(int j=0; j<2; j++) {
            printf("%llu ", fact_matrix[i][j]);
        }
        printf("\n");
    }

    // Tampilkan waktu eksekusi
    double elapsed_time = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    printf("\nWaktu yang dibutuhkan untuk menghitung faktorial setiap elemen matriks: %f detik\n", elapsed_time);

    return 0;
}
```

The explanation :
1. It includes several header files for standard input/output, memory allocation, inter-process communication (IPC), and time measurement.

2. The `calculateFactorial` function calculates the factorial of a given number and returns the result as an unsigned long long integer.

3. In the `main` function, it initializes variables, including a shared memory segment for a 2x2 integer matrix. This shared memory allows communication between processes.

4. It transposes the 2x2 matrix obtained from shared memory and stores the result in the `result` array.

5. It displays the transposed matrix.

6. It calculates the factorial of each element in the `result` matrix and stores the results in the `fact_matrix` array.

7. It displays the `fact_matrix`, showing the factorial of each element.

8. The code measures the time it takes to calculate the factorials using the `clock` function and displays the execution time in seconds.

**Here's the output for rajin.c**

![Output 1](OutputPics/rajin.png)

**The difference between yang.c and belajar.c :**
| `yang.c` (multithreading) | `rajin.c` (single-threaded) |
| ------ | ------ |
|    It uses multithreading with the help of the `pthread` library to calculate factorials in parallel.    | This code calculates the factorials sequentially in a single thread, without using multithreading.       |
| A separate thread is created for each element in the matrix, and each thread calculates the factorial of a specific element concurrently.       | It loops through the matrix elements one by one and computes the factorials sequentially.       |
| This code measures the time taken for the factorial calculations using `clock` and displays the elapsed time, showing the benefits of parallel execution.       | The execution time is also measured using `clock`, but it measures the time for sequential execution, which will typically be longer than the multithreaded approach in `yang.c`.       |

In summary, `yang.c` uses multithreading to perform factorial calculations concurrently, potentially improving performance, while `rajin.c` calculates factorials sequentially in a single thread. The performance of the multithreaded code in `yang.c` is expected to be better, especially when dealing with larger matrices or more computationally intensive tasks.

## Soal 2


**A**

**Read the file and remove non-alphabetic characters from the file, then create an output file named 'thebeatles.txt':**

```bash
//Remove Non-Alphabetic Characters and save to outputFile
void removeSymbols(const char *inputFile, const char *outputFile) {
    FILE *input = fopen(inputFile, "r");
    FILE *output = fopen(outputFile, "w");
    char c;

    if (input == NULL || output == NULL) {
        printf("File Error.\n");
        exit(EXIT_FAILURE);
    }

    while ((c = fgetc(input)) != EOF) {
        if (isalpha(c) || c == ' ' || c == '\n') {
            fputc(c, output);
        }
    }

    fclose(input);
    fclose(output);
}

```
The Function *removeSymbols* is designed to read a file, filter out non-alphabetic characters, and then write the filtered content to a new output file.

The explanation :

a. Opens two file streams using `fopen`:

- input: Opens the inputFile in read mode.

- output: Opens the outputFile in write mode.

b. It checks whether both file streams are successfully opened

c.
It reads the input file character by character using `fgetc`.

- `isalpha(c)`: Checks if the character is alphabetic.
- `c == ' ' || c == '\n'`: Checks for spaces or newline characters.

d. If the character is alphabetic, a space, or a newline, it writes that character to the output file using `fputc`.

**B**

**Calculate the frequency of a specific word from the output file, entered by the user:**

```bash
//Number of times a specific word appears
int countWord(const char *filename, const char *search) {
    FILE *file = fopen(filename, "r");
    int count = 0;
    char word[MAX];

    if (file == NULL) {
        printf("File not found.\n");
        exit(EXIT_FAILURE);
    }

    while (fscanf(file, "%s", word) != EOF) {
        if (strcmp(word, search) == 0) {
            count++;
        }
    }

    fclose(file);
    return count;
}

```
The Function *countWord* is responsible for counting the occurrences of a specific word in a given file.

The Explanation:

a. The function begins by attempting to open the file specified by `filename` using fopen in read mode `r`.

b. Initializes a counter variable `count` to track occurrences of the specified word

c. Using a `while` loop, the function reads words from the file using `fscanf`

d. Compares each word read from the file with the provided `search` word using `strcmp`

e. If a match is found `(if strcmp returns 0)`, it increments the `count` variable to tally the occurrences of the specified word in the file.

```bash
       removeSymbols(OriFile, TargetFile);

       if (strcmp(argv[1], "-kata") == 0) {
           int wordCount = countWord(TargetFile, userInput);
           write(fd1[1], &wordCount, sizeof(wordCount));
```
This code checks if the user input is for counting words `-kata`. It proceeds to count the occurrences of a specific word provided by the user as `userInput`, within the file named `TargetFile`. The `countWord()` function performs this count. Subsequently, the resulting count, stored in the variable `wordCount`, is then written into the pipe fd1[1] using the `write()` function, facilitating communication with the parent process.

```bash
       if (strcmp(argv[1], "-kata") == 0) {
           createLog("KATA", userInput, result, TargetFile);
       } 
```
This code section checks if the command argument is for word counting `-kata`. Upon confirmation, it generates a log entry `KATA` using the `createLog() `function. This log documents the specific word entered by the user as `userInput`, along with the resulting count after the word occurrence calculation within the designated file `TargetFile`. This process occurs within the child process, offering a comprehensive log entry for reference or further analysis.

**C**

**Calculate the frequency of a specific alphabetic character from the output file, entered by the user:**

```bash
int countAlphabet(const char *filename, const char *search) {
    FILE *file = fopen(filename, "r");
    int frequency = 0;
    char ch;

    if (file == NULL) {
        printf("File not found.\n");
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(file)) != EOF) {
        if (ch == search[0]) {
            frequency++;
        }
    }

    fclose(file);
    return frequency;
}
```
The function, `countAlphabet`, aims to count how many times a particular alphabet appears in the provided file.

The Explanation:

a. It takes in the `filename` as the file to be read and `search` as the character to be counted. 

b. The function opens the file in read mode and initializes a counter variable, `frequency`, to track the occurrences of the desired character.

c. Then, it reads the file character by character, incrementing the frequency count each time it encounters the specified character `search[0]`.

d. The function returns the total count of occurrences and closes the file. 

```bash
else if (strcmp(argv[1], "-huruf") == 0) {
           int alphabetCount = countAlphabet(TargetFile, userInput);
           write(fd1[1], &alphabetCount, sizeof(alphabetCount));
       }

```
This code section functions as part of the child process which checks if the command argument is for character counting `-huruf`.
It uses the `countAlphabet `function to determine the frequency of a specific character entered by the user. The function reads the file `TargetFile` and counts the occurrences of the character specified in the `userInput`.

 The obtained `alphabetCount` value, representing the frequency of that character, is then written into `fd1[1]` using write with its size. This information will be communicated to the parent process for further processing and logging.
```bash
else if (strcmp(argv[1], "-huruf") == 0) {
           createLog("HURUF", userInput, result, TargetFile);
       }
```
This code section checks if the command argument is for character counting `-huruf`. Upon confirmation, it generates a log entry `HURUF` using the `createLog() `function. This log documents the specific character entered by the user as `userInput`, along with the resulting count after the character occurrence calculation within the designated file `TargetFile`. This log will record the count and the character based on the user's input. It's part of the overall logging feature, creating a record of the count of the specific character requested in the file.


**D**

**Arguments to run the program:**
```bash
int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        printf("Usage: %s -kata | -huruf\n", argv[0]);
        return EXIT_FAILURE;
    }

    char userInput[MAX];
    
    if (strcmp(argv[1], "-kata") == 0) {
        printf("Enter the word to count: ");
        fgets(userInput, MAX, stdin);
        userInput[strcspn(userInput, "\n")] = '\0';
    } else if (strcmp(argv[1], "-huruf") == 0) {
        printf("Enter the character to count: ");
        fgets(userInput, MAX, stdin);
        userInput[strcspn(userInput, "\n")] = '\0';
    }

```
This main Function receives command-line arguments to decide whether to perform word counting or character counting. 

The Explanation:

a. Checks whether the argument is not either "-kata" or "-huruf". 

b. If the input doesn't match the required format, it displays the correct usage of the program and exits with a failure status.

c. If the argument is `-kata`, it prompts the user to enter a word for counting.

d. If the argument is `-huruf`, it asks the user to enter a single character for counting.

e. Utilizes `fgets()` to read the user input from the command line and remove the newline character.


**E**

**Send the calculation results to the parent process:**
```bash
else { // Parent Process
       close(fd1[1]);
       close(fd2[0]);

       int count;
       read(fd1[0], &count, sizeof(count));

       if (strcmp(argv[1], "-kata") == 0) {
           write(fd2[1], &count, sizeof(count));
       } else if (strcmp(argv[1], "-huruf") == 0) {
           int alphabetCount = countAlphabet(TargetFile, userInput);
           write(fd2[1], &alphabetCount, sizeof(alphabetCount));
       }

       close(fd1[0]);
       close(fd2[1]);

       wait(&status);
   }
```
Code Explanation:

a. Closes the writing end of the first pipe `fd1[1] `and the reading end of the second pipe `fd2[0]` to facilitate communication channels between the processes.

b. Uses `read` to collect the computed value sent by the child process through `fd1[0]` and stores it in the `count `variable.

c. If the argument is `-kata,` writes the word count value to the writing end of the second pipe `fd2[1]`

d. If the argument is `-huruf`, recalculates the alphabet count from the `TargetFile` using the `countAlphabet`function and then writes this value to the second pipe `fd2[1]`.

e. it closes the remaining ends of both pipes to finalize the communication process.

f. Uses wait(&status) to await the completion of the child process.

**F**

`Recording every word or character in a log file ('frekuensi.log'):`
```bash
//Recorded in a log file with format
void createLog(const char *type, const char *item, int count, const char *filename) {
    char timestamp[80];
    time_t t;
    struct tm* tm_now = localtime(&t);
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y %H:%M:%S", tm_now);

    FILE *fileLog = fopen("frekuensi.log", "a");
    if (fileLog != NULL) {
        fprintf(fileLog, "[%s] [%s] %s '%s' muncul sebanyak %d kali dalam file '%s'\n", timestamp, type, (strcmp(type, "KATA") == 0) ? "Kata" : "Huruf", item, count, filename);
        fclose(fileLog);
    }
}
```
The Function *createLog* is responsible for producing formatted log entries in a file named "frekuensi.log". This function is used to maintain a detailed log of word or letter counts

The Explanation:

a. It begins by retrieving the current timestamp using the `localtime()` function, formatting it into a string stored in the `timestamp `variable. 

b. t then opens the "frekuensi.log" file in append mode.

c.  It uses `fprintf()` to write a formatted log entry containing the timestamp, the type ("KATA" for a word count or "HURUF" for a letter count), the specific item being counted, the count or frequency of occurrence, and the corresponding file name where the count occurred.

d. This log entry is constructed based on the input parameters and is written to the log file. 

e. Finally, it closes the log file after writing the entry. 

In summary This code offers a user interface that prompts for a choice between word or character count and processes the respective operation, providing an output log summarizing the counts and operations performed.

#
***Demonstration***

Search for letter/word and printing it to frekuensi.log file: 

![Screenshot_2023-11-03_104311](/uploads/02cac14ab4226a3e92763e65883b902d/Screenshot_2023-11-03_104311.png)

-- **End of Soal 2 explanation** --

**Thank you very much Guysss and Mas Azril >_<**

## Soal 3
This question explores the implementation of interprocess communication using message queues, involving two processes: a sender and a receiver.

#

### Explanation of sender.c

***sender.c***
```
// C Program for Message Queue (Writer Process)
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>

// structure for message queue
struct mesg_buffer
{
    long mesg_type;
    char mesg_text[100];
} message;

int main()
{
    key_t key;
    int msgid;

    // ftok to generate unique key
    key = ftok("besok", 65);

    // msgget
    // either returns the message queue identifier for a newly created message queue
    // or returns the identifiers for a queue which exists with the same key value.
    msgid = msgget(key, 0666 | IPC_CREAT);

    message.mesg_type = 1;

    while (1)
    {
        // printf("Choose from the following commands:\n[CREDS] \n[AUTH: username password] \n[TRANSFER filename]\n\n");
        printf("Input command: ");
        fgets(message.mesg_text, 100, stdin);

        // msgsnd to send message, data is placed on to a message queue
        msgsnd(msgid, &message, sizeof(message), 0);
    }
    return 0;
}
```
The code above defines the sender.c process which operates by:
1. ```key = ftok("besok", 65);```: Generating a unique key
2. ```msgid = msgget(key, 0666 | IPC_CREAT);```: Returning identifier for queue according to the key value
3. ```message.mesg_type = 1;```: Initializing mesg_type
4. ```while(1)```: Running the program as long as it returns true
5. ```fgets(message.mesg_text, 100, stdin);```: Taking user Input
6. ```msgsnd(msgid, &message, sizeof(message), 0);```: Sending message to a message queue
7. ```struct mesg_buffer```: Defining struct for message queue
#

### Explanation of receiver.c

The receiver.c program operates by 4 conditions:
1. **CREDS**: Prints the formatted username and decoded password of users.txt to the receiver.
2. **AUTH: username password**: Find if username and decoded password string exists in users.txt.
3. **TRANSFER filename**: Moves files from 'Sender' directory to 'Receiver' directory.
4. **COMMAND UNKNOWN**: When command is not defined in the previous conditions.

#
***Main Function***
```
int main()
{
    // printf("TEST 1");
    key_t key;
    int msgid;
    int authenticated = 0;

    // ftok to generate unique key
    key = ftok("besok", 65);

    // msgget
    // either returns the message queue identifier for a newly created message queue
    // or returns the identifiers for a queue which exists with the same key value.
    msgid = msgget(key, 0666 | IPC_CREAT);

    // msgrcv to receive message
    // only msg type 1 is received
    // 0: allow program to keep on running without waiting for msg to b available in queue
    while (1)
    {
        // receive type 1 msg as defined in sender
        msgrcv(msgid, &message, sizeof(message), 1, 0);

        // Remove the newline character from the received message
        // bcos fgets includes /n
        message.mesg_text[strcspn(message.mesg_text, "\n")] = '\0';
```

The code above allows the receiver to communicate with the sender by:
1. ```key = ftok("besok", 65);```: Generating unique key that matches sender
2. ```msgid = msgget(key, 0666 | IPC_CREAT);```: Returning identifier for queue according to the key value
3. ```while(1)```: Running the program as long as it returns true
4. ```msgrcv(msgid, &message, sizeof(message), 1, 0);```: Receiving message of type 1, as defined in the sender process
5. ```message.mesg_text[strcspn(message.mesg_text, "\n")] = '\0';```: Removing extra line


#
***"CREDS" command explanation***

```
if (strcmp(message.mesg_text, "CREDS") == 0)
        {
            // open users.txt file
            FILE *file = fopen("/mnt/d/SISOP/sisop/modul-3/soal_3/users.txt", "r");

            if (file)
            {
                char line[1000]; // array to store content
                // printf("TEST 3");

                while (fgets(line, sizeof(line), file))
                {
                    line[strcspn(line, "\n")] = '\0';

                    char *username = strtok(line, ":"); // tokenize based on ":"
                    char *encoded_password = strtok(NULL, ":");

                    // printf("encoded pass: %s\n", encoded_password);

                    if (username && encoded_password)
                    {
                        int string_length = strlen(encoded_password);

                        char *decoded_password = base64decoder(encoded_password, string_length);
                        printf("Username: %s, Password: %s\n", username, decoded_password);
                    }
                }
            }
            fclose(file);
        }
```
How it works:
1. ```if (strcmp(message.mesg_text, "CREDS") == 0)```: Compare if received message to CREDS.
2. ```FILE *file = fopen("/mnt/d/SISOP/sisop/modul-3/soal_3/users.txt", "r");```: Open the file by specifying the path
3. ```while (fgets(line, sizeof(line), file))```: Loop through each line.
4. ```char *username = strtok(line, ":");```: Take text before ":" as username.
5. ```char *encoded_password = strtok(NULL, ":");```: Take text after ":" as password.
6. ```char *decoded_password = base64decoder(encoded_password, string_length);```: Call base64decoder function to decode.
7. ```printf("Username: %s, Password: %s\n", username, decoded_password);```: Print result in receiver.

***base64decoder Function***
```
char *base64decoder(char encoded[], int string_length)
{
    char *decoded_string;
    decoded_string = (char *)malloc(sizeof(char) * 100);

    int i, j, k = 0;

    // stores the bitstream
    int num = 0;

    // count_bits stores current number of bits in num
    int count_bits = 0;

    // selects 4 characters from encoded string at a time
    // find the position of each encoded character in char_set and stores in num
    for (i = 0; i < string_length; i += 4)
    {
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++)
        {
            // make space for 6 bits.
            if (encoded[i + j] != '=')
            {
                num = num << 6;
                count_bits += 6;
            }

            /* Finding the position of each encoded
            character in char_set
            and storing in "num", use OR
            '|' operator to store bits*/

            // encoded[i + j] = 'E', 'E' - 'A' = 5
            // 'E' has 5th position in char_set
            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');

            // encoded[i + j] = 'e', 'e' - 'a' = 5,
            // 5 + 26 = 31, 'e' has 31st position in char_set
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);

            // encoded[i + j] = '8', '8' - '0' = 8
            // 8 + 52 = 60, '8' has 60th position in char_set
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);

            // '+' occurs in 62nd position in char_set
            else if (encoded[i + j] == '+')
                num = num | 62;

            // '/' occurs in 63rd position in char_set
            else if (encoded[i + j] == '/')
                num = num | 63;

            // ( str[i + j] == '=' ) remove 2 bits
            // which delete appended bits during encoding
            else
            {
                num = num >> 2;
                count_bits -= 2;
            }
        }

        while (count_bits != 0)
        {
            count_bits -= 8;

            // 255 in binary is 11111111
            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }

    // place NULL character to mark end of string
    decoded_string[k] = '\0';

    return decoded_string;
}
```
Referenced from https://www.geeksforgeeks.org/decode-encoded-base-64-string-ascii-string/


#
***"AUTH: username password" command explanation***
```
else if (strncmp(message.mesg_text, "AUTH: ", 6) == 0)
        {
            char *auth_message = message.mesg_text + 6; // move pointer past "AUTH: "

            // printf("AUTH works\n");

            char *auth_username = strtok(auth_message, " ");
            char *auth_password = strtok(NULL, "");

            if (auth_username && auth_password)
            {
                int status = 0;
                FILE *file = fopen("/mnt/d/SISOP/sisop/modul-3/soal_3/users.txt", "r");

                if (file)
                {
                    char line[1000];

                    while (fgets(line, sizeof(line), file))
                    {
                        line[strcspn(line, "\n")] = '\0';

                        char *username = strtok(line, ":");
                        char *encoded_password = strtok(NULL, ":");

                        if (username && encoded_password)
                        {
                            int string_length = strlen(encoded_password);
                            char *decoded_password = base64decoder(encoded_password, string_length);

                            if (strcmp(auth_username, username) == 0 && strcmp(auth_password, decoded_password) == 0)
                            {
                                status = 1;
                                break;
                            }

                            free(decoded_password);
                        }
                    }
                    fclose(file);
                }
                if (status)
                {
                    printf("Authentication successful\n");
                    authenticated = 1;
                }
                else
                {
                    printf("Authentication failed\n");
                    authenticated = 0;
                    msgctl(msgid, IPC_RMID, NULL);
                    return 0;
                }
            }
        }
```
How it works:
1. ```char *auth_username = strtok(auth_message, " ");```: Get inputted username
2. ```char *auth_password = strtok(NULL, "");```: Get inputted password
3. Get username and password, method similar to CREDS
4. ```if (strcmp(auth_username, username) == 0 && strcmp(auth_password, decoded_password) == 0)```: Compare if inputted username and password matches with users.txt
5. ```status = 1; break;```: If matches, return 1.
6. Update authenticated variable and print message according to result. Destroy program if authentication failed.
```
if (status)
                {
                    printf("Authentication successful\n");
                    authenticated = 1;
                }
                else
                {
                    printf("Authentication failed\n");
                    authenticated = 0;
                    msgctl(msgid, IPC_RMID, NULL);
                    return 0;
                }
```


#
***"TRANSFER filename" command explanation***
```
        else if (strncmp(message.mesg_text, "TRANSFER ", 9) == 0)
        {

            if (authenticated == 1)
            {
                char *senderFile = message.mesg_text + 9; // move pointer past "TRANSFER "

                char source[300];
                char destination[300];

                sprintf(source, "Sender/%s", senderFile);
                sprintf(destination, "Receiver/%s", senderFile);

                struct stat st;
                if (stat(destination, &st) == 0)
                {
                    printf("Found a duplicate file in 'Receiver' directory\n");
                }
                else
                {
                    // Move the file from source to destination.
                    if (rename(source, destination) == 0)
                    {
                        if (stat(destination, &st) == 0)
                        {
                            printf("Moved file size: %lld KB\n", (long long)st.st_size / 1024);
                        }
                        else
                        {
                            perror("Error getting file size");
                        }
                    }
                    else
                    {
                        perror("cant move file");
                    }
                }
            }
            else
            {
                printf("not auth");
            }
        }
        else
        {
            printf("UNKNOWN COMMAND\n");
            fflush(stdout);
        }
    }
```
How it works:
1. ```if (authenticated == 1)```: Only allow authenticated users to perform TRANSFER command.
2. ```char *senderFile = message.mesg_text + 9;```
3. Define source path and destination path.
```
sprintf(source, "Sender/%s", senderFile);
sprintf(destination, "Receiver/%s", senderFile);
```
4. stat function collects information about file and checks if there are any duplicates.
```
struct stat st;
                if (stat(destination, &st) == 0)
                {
                    printf("Found a duplicate file in 'Receiver' directory\n");
                }
```
5. If there are no duplicates, move the file.
```
else
                {
                    // Move the file from source to destination.
                    if (rename(source, destination) == 0)
                    {
                        if (stat(destination, &st) == 0)
                        {
                            printf("Moved file size: %lld KB\n", (long long)st.st_size / 1024);
                        }
```
6. Include error handling.

#
***UKNOWN COMMAND***
```
else
        {
            printf("UNKNOWN COMMAND\n");
            fflush(stdout);
        }
```
When the 3 conditions are not met, the program prints "UNKNOWN COMMAND".

#
***Destroy message queue***
```
msgctl(msgid, IPC_RMID, NULL);
```
Outside while true loop.

#
***Demonstration***

Inside 'Receiver' directory and 'Sender' directory (before):

![Screenshot_2023-10-30_181959](/uploads/b69d69bb44250c9467fc64ff3698a0b1/Screenshot_2023-10-30_181959.png)

Information of file size:

![Screenshot_2023-10-30_182035](/uploads/881376335d9e3b56a7f3deab5f3b2f8b/Screenshot_2023-10-30_182035.png)

Tried all command (all works):

![Screenshot_2023-10-30_185941](/uploads/28e7e069b04507a7de6ef465435498fb/Screenshot_2023-10-30_185941.png)

Inside 'Receiver' directory and 'Sender' directory (after):

![Screenshot_2023-10-30_185735](/uploads/57fd04ba77f8d5e3639c945bb7d6d337/Screenshot_2023-10-30_185735.png)

-- **End of Soal 3 explanation** --

## Soal 4

#
**Takumi is an intern at Evil Corp. He has been assigned the task of creating a public chat room using socket concepts. The detailed requirements are as follows:**

1. Clients and the server are connected through sockets.
2. The server acts as a message receiver from clients and only displays the messages.
3. Due to limited resources at Evil Corp, the server should only be able to establish connections with up to 5 clients.

**server.c**

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <pthread.h>

#define PORT 12345
#define MAX_CLIENTS 5

int clients[MAX_CLIENTS];
int num_clients = 0;
pthread_t threads[MAX_CLIENTS];

void *handle_client(void *client_socket) {
    int client_sock = *(int *)client_socket;
    char buffer[1024];
    int bytes_received;

    while ((bytes_received = recv(client_sock, buffer, sizeof(buffer), 0)) > 0) {
        buffer[bytes_received] = '\0';
        printf("Client %d: %s", client_sock-3, buffer);

        for (int i = 0; i < num_clients; i++) {
            if (clients[i] != client_sock) {
                send(clients[i], buffer, strlen(buffer), 0);
            }
        }
    }

    close(client_sock);
    for (int i = 0; i < num_clients; i++) {
        if (clients[i] == client_sock) {
            for (int j = i; j < num_clients - 1; j++) {
                clients[j] = clients[j + 1];
            }
            num_clients--;
            break;
        }
    }

    pthread_exit(NULL);
}

int main() {
    int server_socket, client_socket;
    struct sockaddr_in server_addr, client_addr;
    socklen_t addr_size;

    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket < 0) {
        perror("Error in socket");
        exit(1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = PORT;
    server_addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("Error in binding");
        exit(1);
    }

    if (listen(server_socket, 10) == 0) {
        printf("Listening...\n");
    } else {
        perror("Error in listening");
        exit(1);
    }

    while (1) {
        if (num_clients < MAX_CLIENTS) {
            client_socket = accept(server_socket, (struct sockaddr *)&client_addr, &addr_size);
            clients[num_clients] = client_socket;
            num_clients++;

            pthread_create(&threads[num_clients - 1], NULL, handle_client, &client_socket);
        }
    }

    return 0;
}
```
Server.c code is an example of a simple multi-client chat server using sockets and pthreads. It sets up a server that can handle multiple client connections and relay messages between clients.

1. Include and Define:
   - It defines a `PORT` for the server to listen on and `MAX_CLIENTS` to limit the maximum number of clients the server can handle.

2. `int clients[MAX_CLIENTS]` and `int num_clients = 0` are arrays to keep track of client sockets and the current number of connected clients.

3. `pthread_t threads[MAX_CLIENTS]` is an array to store thread identifiers for each client.

4. `void *handle_client(void *client_socket)` is the function that will be executed in a separate thread for each client. It handles the communication with the client.

5. In the `handle_client` function:
   - `int client_sock = *(int *)client_socket;` extracts the client socket from the argument passed to the thread.
   - A buffer is used to receive and send messages, and the `recv` function is used to receive data from the client.
   - The received data is printed with the associated client ID (calculated as `client_sock - 3`).
   - The received message is broadcasted to all other connected clients except the sender.

6. In the `main` function:
   - The server socket and client socket variables are declared.
   - `server_socket` is created using `socket()` to create a socket with IPv4 and TCP properties.
   - The code checks if the socket creation was successful and handles errors.
   - Server address (`server_addr`) is configured with the specified port and IP address `INADDR_ANY`, which allows the server to listen on all available network interfaces.

7. The server socket is bound to the specified address using `bind()`. Any binding error is handled.

8. The server enters the listening state with a maximum backlog of 10 pending connections using `listen()`. Errors are checked and handled accordingly.

9. The server enters an infinite loop to accept client connections. It checks if the number of clients is less than the maximum allowed (`MAX_CLIENTS`) and accepts incoming connections using `accept()`. Each new client socket is stored in the `clients` array and a new thread is created to handle the client using `pthread_create`.

10. In each thread created for a client, the `handle_client` function manages communication with the respective client.

This code serves as a simple multi-client chat server where messages sent by one client are relayed to all other connected clients.

**Client.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <pthread.h>

#define PORT 12345


int main() {
    int client_socket;
    struct sockaddr_in server_addr;

    client_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (client_socket < 0) {
        perror("Error in socket");
        exit(1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = PORT;
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    if (connect(client_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("Error in connection");
        exit(1);
    }

    char message[1024];
    while (1) {
        printf("Send message: ");
        fgets(message, sizeof(message), stdin);
        send(client_socket, message, strlen(message), 0);
    }

    close(client_socket);

    return 0;
}

```
This code is an example program in the C programming language that creates a simple client to communicate with a server using a socket connection. The code includes various header files and built-in C library functions to set up a socket connection to the server and send messages.

Here's a more detailed explanation of each part of the code:

1. Include and Define:
   - `#define PORT 12345` defines the port number that will be used for the server connection. Port 12345 is used in this example, but you can change it as needed.

2. `int main()`: The main function of the program starts here.

3. `int client_socket;` and `struct sockaddr_in server_addr;` are variable declarations for the client socket and a structure to manage the server's address.

4. `client_socket = socket(AF_INET, SOCK_STREAM, 0);` is a function call to create a socket. `AF_INET` indicates the use of the IPv4 protocol, `SOCK_STREAM` indicates the type of socket used for TCP connections, and `0` is a protocol parameter left empty.

5. The code then checks whether the socket was created successfully. If not, an error message is printed, and the program exits with an error status.

6. Server address configuration:
   - `server_addr.sin_family = AF_INET;` sets the address type to IPv4.
   - `server_addr.sin_port = PORT;` sets the port number defined earlier.
   - `server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");` sets the server's IP address to 127.0.0.1 (localhost). You can change this according to the actual server address.

7. The code then attempts to connect the client socket to the server using `connect`. If the connection fails, an error message is printed, and the program exits with an error status.

8. Next, the program enters an infinite loop that allows the user to input messages to be sent to the server. Messages are sent via the socket using `send`. The message sent is user input read with `fgets`.

9. This infinite loop can only be exited by closing the client socket with `close(client_socket)`.

10. Finally, the program returns a value of 0 from `main()` and terminates.

This code is an example of a simple client for sending messages to a server via a TCP socket connection. To complete this application, you need to have a server that can accept connections from this client and respond to the messages sent.

**Communication between one server and 5 clients:**

![Screenshot_2023-11-03_013359](/uploads/6b3a1128c9e1802ff283ab126ffc742d/Screenshot_2023-11-03_013359.png)
