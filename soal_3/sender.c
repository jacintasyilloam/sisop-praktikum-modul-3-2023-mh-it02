// C Program for Message Queue (Writer Process)
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>

// structure for message queue
struct mesg_buffer
{
    long mesg_type;
    char mesg_text[100];
} message;

int main()
{
    key_t key;
    int msgid;

    // ftok to generate unique key
    key = ftok("besok", 65);

    // msgget
    // either returns the message queue identifier for a newly created message queue
    // or returns the identifiers for a queue which exists with the same key value.
    msgid = msgget(key, 0666 | IPC_CREAT);

    message.mesg_type = 1;

    while (1)
    {
        // printf("Choose from the following commands:\n[CREDS] \n[AUTH: username password] \n[TRANSFER filename]\n\n");
        printf("Input command: ");
        fgets(message.mesg_text, 100, stdin);

        // msgsnd to send message, data is placed on to a message queue
        msgsnd(msgid, &message, sizeof(message), 0);
    }
    return 0;
}
