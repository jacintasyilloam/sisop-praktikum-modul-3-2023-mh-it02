// C Program for Message Queue (Reader Process)
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

// struct for message queue
struct mesg_buffer
{
    long mesg_type;
    char mesg_text[100];
} message;

// function to do base64 decoding
char *base64decoder(char encoded[], int string_length)
{
    char *decoded_string;
    decoded_string = (char *)malloc(sizeof(char) * 100);

    int i, j, k = 0;

    // stores the bitstream
    int num = 0;

    // count_bits stores current number of bits in num
    int count_bits = 0;

    // selects 4 characters from encoded string at a time
    // find the position of each encoded character in char_set and stores in num
    for (i = 0; i < string_length; i += 4)
    {
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++)
        {
            // make space for 6 bits.
            if (encoded[i + j] != '=')
            {
                num = num << 6;
                count_bits += 6;
            }

            /* Finding the position of each encoded
            character in char_set
            and storing in "num", use OR
            '|' operator to store bits*/

            // encoded[i + j] = 'E', 'E' - 'A' = 5
            // 'E' has 5th position in char_set
            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');

            // encoded[i + j] = 'e', 'e' - 'a' = 5,
            // 5 + 26 = 31, 'e' has 31st position in char_set
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);

            // encoded[i + j] = '8', '8' - '0' = 8
            // 8 + 52 = 60, '8' has 60th position in char_set
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);

            // '+' occurs in 62nd position in char_set
            else if (encoded[i + j] == '+')
                num = num | 62;

            // '/' occurs in 63rd position in char_set
            else if (encoded[i + j] == '/')
                num = num | 63;

            // ( str[i + j] == '=' ) remove 2 bits
            // which delete appended bits during encoding
            else
            {
                num = num >> 2;
                count_bits -= 2;
            }
        }

        while (count_bits != 0)
        {
            count_bits -= 8;

            // 255 in binary is 11111111
            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }

    // place NULL character to mark end of string
    decoded_string[k] = '\0';

    return decoded_string;
}

int main()
{
    // printf("TEST 1");
    key_t key;
    int msgid;
    int authenticated = 0;

    // ftok to generate unique key
    key = ftok("besok", 65);

    // msgget
    // either returns the message queue identifier for a newly created message queue
    // or returns the identifiers for a queue which exists with the same key value.
    msgid = msgget(key, 0666 | IPC_CREAT);

    // msgrcv to receive message
    // only msg type 1 is received
    // 0: allow program to keep on running without waiting for msg to b available in queue
    while (1)
    {
        // receive type 1 msg as defined in sender
        msgrcv(msgid, &message, sizeof(message), 1, 0);

        // Remove the newline character from the received message
        // bcos fgets includes /n
        message.mesg_text[strcspn(message.mesg_text, "\n")] = '\0';

        if (strcmp(message.mesg_text, "CREDS") == 0)
        {
            // open users.txt file
            FILE *file = fopen("/mnt/d/SISOP/sisop/modul-3/soal_3/users.txt", "r");

            if (file)
            {
                char line[1000]; // array to store content
                // printf("TEST 3");

                while (fgets(line, sizeof(line), file))
                {
                    line[strcspn(line, "\n")] = '\0';

                    char *username = strtok(line, ":"); // tokenize based on ":"
                    char *encoded_password = strtok(NULL, ":");

                    // printf("encoded pass: %s\n", encoded_password);

                    if (username && encoded_password)
                    {
                        int string_length = strlen(encoded_password);

                        char *decoded_password = base64decoder(encoded_password, string_length);
                        printf("Username: %s, Password: %s\n", username, decoded_password);
                    }
                }
            }
            fclose(file);
        }

        else if (strncmp(message.mesg_text, "AUTH: ", 6) == 0)
        {
            char *auth_message = message.mesg_text + 6; // move pointer past "AUTH: "

            // printf("AUTH works\n");

            char *auth_username = strtok(auth_message, " ");
            char *auth_password = strtok(NULL, "");

            if (auth_username && auth_password)
            {
                int status = 0;
                FILE *file = fopen("/mnt/d/SISOP/sisop/modul-3/soal_3/users.txt", "r");

                if (file)
                {
                    char line[1000];

                    while (fgets(line, sizeof(line), file))
                    {
                        line[strcspn(line, "\n")] = '\0';

                        char *username = strtok(line, ":");
                        char *encoded_password = strtok(NULL, ":");

                        if (username && encoded_password)
                        {
                            int string_length = strlen(encoded_password);
                            char *decoded_password = base64decoder(encoded_password, string_length);

                            if (strcmp(auth_username, username) == 0 && strcmp(auth_password, decoded_password) == 0)
                            {
                                status = 1;
                                break;
                            }

                            free(decoded_password);
                        }
                    }
                    fclose(file);
                }
                if (status)
                {
                    printf("Authentication successful\n");
                    authenticated = 1;
                }
                else
                {
                    printf("Authentication failed\n");
                    authenticated = 0;
                    msgctl(msgid, IPC_RMID, NULL);
                    return 0;
                }
            }
        }

        else if (strncmp(message.mesg_text, "TRANSFER ", 9) == 0)
        {

            if (authenticated == 1)
            {
                char *senderFile = message.mesg_text + 9; // move pointer past "TRANSFER "

                char source[300];
                char destination[300];

                sprintf(source, "Sender/%s", senderFile);
                sprintf(destination, "Receiver/%s", senderFile);

                struct stat st;
                if (stat(destination, &st) == 0)
                {
                    printf("Found a duplicate file in 'Receiver' directory\n");
                }
                else
                {
                    // Move the file from source to destination.
                    if (rename(source, destination) == 0)
                    {
                        if (stat(destination, &st) == 0)
                        {
                            printf("Moved file size: %lld KB\n", (long long)st.st_size / 1024);
                        }
                        else
                        {
                            perror("Error getting file size");
                        }
                    }
                    else
                    {
                        perror("cant move file");
                    }
                }
            }
            else
            {
                printf("not auth");
            }
        }
        else
        {
            printf("UNKNOWN COMMAND\n");
            fflush(stdout);
        }
    }

    // to destroy the message queue
    msgctl(msgid, IPC_RMID, NULL);
    return 0;
}
