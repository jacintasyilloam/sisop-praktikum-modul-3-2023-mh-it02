#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define MAX 1000
#define OriFile "/home/userzara/Documents/Modul3/lirik.txt"
#define TargetFile "thebeatles.txt"

//Remove Non-Alphabetic Characters and save to outputFile
void removeSymbols(const char *inputFile, const char *outputFile) {
    FILE *input = fopen(inputFile, "r");
    FILE *output = fopen(outputFile, "w");
    char c;

    if (input == NULL || output == NULL) {
        printf("File Error.\n");
        exit(EXIT_FAILURE);
    }

    while ((c = fgetc(input)) != EOF) {
        if (isalpha(c) || c == ' ' || c == '\n') {
            fputc(c, output);
        }
    }

    fclose(input);
    fclose(output);
}

//Number of times a specific word appears
int countWord(const char *filename, const char *search) {
    FILE *file = fopen(filename, "r");
    int count = 0;
    char word[MAX];

    if (file == NULL) {
        printf("File not found.\n");
        exit(EXIT_FAILURE);
    }

    while (fscanf(file, "%s", word) != EOF) {
        if (strcmp(word, search) == 0) {
            count++;
        }
    }

    fclose(file);
    return count;
}

//Number of times a specific alphabetic character appears
int countAlphabet(const char *filename, const char *search) {
    FILE *file = fopen(filename, "r");
    int frequency = 0;
    char ch;

    if (file == NULL) {
        printf("File not found.\n");
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(file)) != EOF) {
        if (ch == search[0]) {
            frequency++;
        }
    }

    fclose(file);
    return frequency;
}

//Recorded in a log file with format
void createLog(const char *type, const char *item, int count, const char *filename) {
    char timestamp[80];
    time_t t;
    struct tm* tm_now = localtime(&t);
    strftime(timestamp, sizeof(timestamp), "%d/%m/%Y %H:%M:%S", tm_now);

    FILE *fileLog = fopen("frekuensi.log", "a");
    if (fileLog != NULL) {
        fprintf(fileLog, "[%s] [%s] %s '%s' muncul sebanyak %d kali dalam file '%s'\n", timestamp, type, (strcmp(type, "KATA") == 0) ? "Kata" : "Huruf", item, count, filename);
        fclose(fileLog);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2 || (strcmp(argv[1], "-kata") != 0 && strcmp(argv[1], "-huruf") != 0)) {
        printf("Usage: %s -kata | -huruf\n", argv[0]);
        return EXIT_FAILURE;
    }

    char userInput[MAX];
    
    if (strcmp(argv[1], "-kata") == 0) {
        printf("Enter the word to count: ");
        fgets(userInput, MAX, stdin);
        userInput[strcspn(userInput, "\n")] = '\0';
    } else if (strcmp(argv[1], "-huruf") == 0) {
        printf("Enter the character to count: ");
        fgets(userInput, MAX, stdin);
        userInput[strcspn(userInput, "\n")] = '\0';
    }

    int fd1[2]; // pipe 1
    int fd2[2]; // pipe 2
    int status;
    int result;

   if (pipe(fd1) == -1 || pipe(fd2) == -1) {
       perror("pipe");
       exit(EXIT_FAILURE);
   }

   pid_t pid = fork(); //Creating child process

   if (pid < 0) {
       perror("fork");
       exit(EXIT_FAILURE);
   } else if (pid == 0) { // Child Process
       close(fd1[0]);
       close(fd2[1]);

       removeSymbols(OriFile, TargetFile);

       if (strcmp(argv[1], "-kata") == 0) {
           int wordCount = countWord(TargetFile, userInput);
           write(fd1[1], &wordCount, sizeof(wordCount));

       } else if (strcmp(argv[1], "-huruf") == 0) {
           int alphabetCount = countAlphabet(TargetFile, userInput);
           write(fd1[1], &alphabetCount, sizeof(alphabetCount));
       }

       read(fd2[0], &result, sizeof(result));

       if (strcmp(argv[1], "-kata") == 0) {
           createLog("KATA", userInput, result, TargetFile);
       } else if (strcmp(argv[1], "-huruf") == 0) {
           createLog("HURUF", userInput, result, TargetFile);
       }

       close(fd1[1]);
       close(fd2[0]);

    } else { // Parent Process
       close(fd1[1]);
       close(fd2[0]);

       int count;
       read(fd1[0], &count, sizeof(count));

       if (strcmp(argv[1], "-kata") == 0) {
           write(fd2[1], &count, sizeof(count));
       } else if (strcmp(argv[1], "-huruf") == 0) {
           int alphabetCount = countAlphabet(TargetFile, userInput);
           write(fd2[1], &alphabetCount, sizeof(alphabetCount));
       }

       close(fd1[0]);
       close(fd2[1]);

       wait(&status);
   }
   return EXIT_SUCCESS;
}
