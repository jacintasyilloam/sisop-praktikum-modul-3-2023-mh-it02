#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <pthread.h>

#define PORT 12345
#define MAX_CLIENTS 5

int clients[MAX_CLIENTS];
int num_clients = 0;
pthread_t threads[MAX_CLIENTS];

void *handle_client(void *client_socket) {
    int client_sock = *(int *)client_socket;
    char buffer[1024];
    int bytes_received;

    while ((bytes_received = recv(client_sock, buffer, sizeof(buffer), 0)) > 0) {
        buffer[bytes_received] = '\0';
        printf("Client %d: %s", client_sock-3, buffer);

        for (int i = 0; i < num_clients; i++) {
            if (clients[i] != client_sock) {
                send(clients[i], buffer, strlen(buffer), 0);
            }
        }
    }

    close(client_sock);
    for (int i = 0; i < num_clients; i++) {
        if (clients[i] == client_sock) {
            for (int j = i; j < num_clients - 1; j++) {
                clients[j] = clients[j + 1];
            }
            num_clients--;
            break;
        }
    }

    pthread_exit(NULL);
}

int main() {
    int server_socket, client_socket;
    struct sockaddr_in server_addr, client_addr;
    socklen_t addr_size;

    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket < 0) {
        perror("Error in socket");
        exit(1);
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = PORT;
    server_addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        perror("Error in binding");
        exit(1);
    }

    if (listen(server_socket, 10) == 0) {
        printf("Listening...\n");
    } else {
        perror("Error in listening");
        exit(1);
    }

    while (1) {
        if (num_clients < MAX_CLIENTS) {
            client_socket = accept(server_socket, (struct sockaddr *)&client_addr, &addr_size);
            clients[num_clients] = client_socket;
            num_clients++;

            pthread_create(&threads[num_clients - 1], NULL, handle_client, &client_socket);
        }
    }

    return 0;
}
