#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

//matriks 2x2 karena it02

int main() {
    srand(time(NULL));

    int matrix1[2][2]; //masukin nilai baris kolom matriks1
    int matrix2[2][2]; //masukin nilai baris kolom matriks2
    int result[2][2]; //hasilnya ordo 2x2

    // matriks 1 dimana angkanya 1-4
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            matrix1[i][j] = rand() % 4 + 1;
        }
    }
    
    printf("Matrix 1:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d ", matrix1[i][j]);
        }
        printf("\n");
    }

    // matriks 1 dimana angkanya 1-5
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            matrix2[i][j] = rand() % 5 + 1;
        }
    }
    
    printf("\nMatrix 2:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    }

    // perkalian matriks ordo 2x2
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            result[i][j] = 0;
            for (int k = 0; k < 2; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }
    
    //hasil A
    printf("\nResult matriks 2 x 2:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    // hasil B dari perkalian tersebut dikurang 1 di setiap matriks
    printf("\nMatriks setelah dikurang 1:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d ", result[i][j] - 1); 
        }
        printf("\n");
    }

    // create shared memory biar kesimpen di yang.c sama rajin.c
    key_t key = 1234;
    int shmid = shmget(key, sizeof(result), 0666 | IPC_CREAT);
    int (*shared_result)[2] = shmat(shmid, NULL, 0);

    // Copy the result to shared memory after -1
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            shared_result[i][j] = result[i][j] - 1;
        }
    }

    // ngelepasin ke share memory
    shmdt(shared_result);

    return 0;
}
