#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <pthread.h>

struct thread {
    int row;
    int col;
    int value;
};

unsigned long long factorial_matrix[2][2];

void *calculateFactorial(void *arg) {
    struct thread *element = (struct thread *)arg;
    unsigned long long fact = 1;

    for (int i = 1; i <= element->value; i++) {
        fact *= i;
    }

    factorial_matrix[element->row][element->col] = fact;
    return NULL;
}

void calculate_factorial(pthread_t tid[2][2], struct thread thread_args[2][2]) {
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            pthread_create(&tid[i][j], NULL, calculateFactorial, &thread_args[i][j]);
        }
    }

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            pthread_join(tid[i][j], NULL);
        }
    }
}

int main() {
    int shmid;
    int size = sizeof(int[2][2]);
    key_t key = 1234;

    shmid = shmget(key, size, 0666);
    int (*shared_matrix)[2] = shmat(shmid, NULL, 0);

    int result[2][2];
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            result[i][j] = shared_matrix[j][i]; //dibalik soalnya untuk transpose
        }
    }
    shmdt(shared_matrix);

    // Print hasil matrix
    printf("Matriks:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    pthread_t tid[2][2];
    struct thread thread_args[2][2];
    
    //menghitung faktorial 
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            thread_args[i][j].row = i;
            thread_args[i][j].col = j;
            thread_args[i][j].value = result[i][j];
        }
    }

    // untuk hitung waktunya
    clock_t start, end;
    double cpu_time_used;
    start = clock();
    calculate_factorial(tid, thread_args);
    end = clock();

    // untuk hitung waktunya
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("\nMaka:\n");
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("%llu ", factorial_matrix[i][j]);
        }
        printf("\n");
    }

    printf("\nWaktu yang dibutuhkan untuk menghitung faktorial setiap elemen matriks: %f detik\n", cpu_time_used);

    return 0;
}