#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

// Fungsi untuk menghitung faktorial
unsigned long long calculateFactorial(int num) {
    unsigned long long result = 1;
    for (int i = 1; i <= num; i++) {
        result *= i;
    }
    return result;
}

int main() {
    int shmid;
    int size = sizeof(int[2][2]);
    key_t key = 1234;

    shmid = shmget(key, size, 0666);
    int (*shared_matrix)[2] = (int (*)[2]) shmat(shmid, NULL, 0);

    int result[2][2];
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            result[i][j] = shared_matrix[j][i]; //ditukar biar ke transpose
        }
    }
    shmdt(shared_matrix);

    // Tampilkan matriks hasil perkalian
    printf("Hasil perkalian matriks:\n");
    for(int i=0; i<2; i++) {
        for(int j=0; j<2; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    unsigned long long fact_matrix[2][2];
    clock_t start_time, end_time;
    start_time = clock();

    for(int i=0; i<2; i++) {
        for(int j=0; j<2; j++) {
            fact_matrix[i][j] = calculateFactorial(result[i][j]);
        }
    }
    end_time = clock();

    // hasil faktorial dalam matriks
    printf("\nHasil faktorial matriks:\n");
    for(int i=0; i<2; i++) {
        for(int j=0; j<2; j++) {
            printf("%llu ", fact_matrix[i][j]);
        }
        printf("\n");
    }

    // Tampilkan waktu eksekusi
    double elapsed_time = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
    printf("\nWaktu yang dibutuhkan untuk menghitung faktorial setiap elemen matriks: %f detik\n", elapsed_time);

    return 0;
}